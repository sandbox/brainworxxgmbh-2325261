<?php
/**
 * @file
 * Krexx: Krumo eXXtended.
 *
 * Krexx is a debugging tool, which displays structured information
 * about any PHP object. It is a nice replacement for print_r() or var_dump()
 * which are used by a lot of PHP developers.
 * @author brainworXX GmbH <info@brainworxx.de>
 *
 * Krexx is a fork of Krumo, which was originally written by:
 * Kaloyan K. Tsvetkov <kaloyan@kaloyan.info>
 *
 * @package Krexx
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function includekrexx_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.includekrexx':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("kreXX is a php debug tool, which displays debug information about objects and variables in it's own draggable debug output.") . '</p>';
      $output .= '<h3>' . t('Main analytic functions:') . '</h3>';
      $output .= '<strong>krexx($myObject)</strong>;<br/>';
      $output .= t('Most of the time you will use this method. kreXX will analyse the $object and give you detailed information about it:');
      $output .= '<ul>';
      $output .= '<li>' . t('Public attributes of an objects') . '</li>';
      $output .= '<li>' . t('Protected attributes of an objects (can be toggled off)') . '</li>';
      $output .= '<li>' . t('Private attributes of an objects (can be toggled off)') . '</li>';
      $output .= '<li>' . t('Content of the traversable part of an object (can be toggled off)') . '</li>';
      $output .= '<li>' . t('Result of all configured debugging callbacks (can be configured)') . '</li>';
      $output .= '</ul>';
      $output .= t('This is an alias for \kreXX::open($myObject);') . '<br/>';
      $output .= t('In case the variable is not an object, it will simply display its content.') . '<br/><br/>';

      $output .= '<strong>\krexx::timerStart();</strong><br/>';
      $output .= t('Starts a benchmark') . '<br/><br/>';

      $output .= '<strong>\krexx::timerMoment($string);</strong><br/>';
      $output .= t('Defines a "moment" during a benchmark test.') . '<br/>';
      $output .= t('The $string should be something meaningfull, like "Model invoice db call".') . '<br/><br/>';

      $output .= '<strong>\krexx::timerEnd();</strong><br/>';
      $output .= t('Ends the benchmark test and displays the result.') . '<br/><br/>';

      $output .= '<strong>\krexx::backtrace();</strong><br/>';
      $output .= t('Outputs a standard backtrace. When kreXX finds objects inside the backtrace, it will analyse them.') . '<br/><br/>';

      $output .= '<strong>\krexx::registerFatal();</strong><br/>';
      $output .= t('Registers the fatal error handler in the system.') . '<br/><br/>';

      $output .= '<strong>\krexx::unregisterFatal();</strong><br/>';
      $output .= t('Unregisters the fatal error handler in the system.') . '<br/><br/>';

      $output .= '<p>' . t('Further documentation can be found here:') . '<br/><a target="_blank" href="https://sourceforge.net/p/krexx/wiki/Overview/">https://sourceforge.net/p/krexx/wiki/Overview/</a><p>';
      return $output;
  }
}
