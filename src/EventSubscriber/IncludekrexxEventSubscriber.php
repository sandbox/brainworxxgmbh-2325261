<?php
/**
 * @file
 * Krexx: Krumo eXXtended.
 *
 * Krexx is a debugging tool, which displays structured information
 * about any PHP object. It is a nice replacement for print_r() or var_dump()
 * which are used by a lot of PHP developers.
 * @author brainworXX GmbH <info@brainworxx.de>
 *
 * Krexx is a fork of Krumo, which was originally written by:
 * Kaloyan K. Tsvetkov <kaloyan@kaloyan.info>
 *
 * @package Krexx
 */

namespace Drupal\includekrexx\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class IncludekrexxEventSubscriber implements EventSubscriberInterface {

  /**
   * Initializes kreXX module requirements.
   */
  public function onRequest(GetResponseEvent $event) {
    if (!class_exists('Krexx')) {
      include_once 'modules/includekrexx/kreXX/kreXX.php';
    }
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    // Include it as early as possible.
    $events[KernelEvents::REQUEST][] = array('onRequest', -101);

    return $events;
  }
}
