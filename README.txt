CONTENTS OF THIS FILE
---------------------
* Introduction
* Configuration
* Troubleshooting
* FAQ
* Maintainers

INTRODUCTION
------------
kreXX is a php debug tool, which displays debug information
about objects and variables in it's own draggable debug output.

CONFIGURATION
-------------
kreXX can be configured, but that is not necessary
1.) You can edit all settings in the outputwindow of your browser.
    The settings will be stored in a cookie. There are helptexts
    available to explain what all configuration options do.
2.) When no local browser settigns are available, kreXX will read
    it's settings from a ini-file. kreXX tells you in the footer
    where it is looking for it. We've included an example file
    in the krexx directory, where we have exolained all possible
    settings.
3.) When kreXX can not locate a config file, it will fallback to
    it's factory settings.

MAIN ANALYTIC FUNCTIONS (FAQ)
-----------------------------
krexx($myObject);
Most of the time you will use this method. kreXX will analyse
the $object and give you detailed information about it:
* Public attributes of an objects
* Protected attributes of an objects (can be toggled off)
* Private attributes of an objects (can be toggled off)
* Content of the traversable part of an object (can be toggled off)
* Result of all configured debugging callbacks (can be configured)
This is an alias for \kreXX::open($myObject);
In case the variable is not an object, it will simply display its content.

\kreXX::timerStart();
Starts a benchmark

\kreXX::timerMoment($string);
Defines a "moment" during a benchmark test.
The $string should be something meaningfull,
like "Model invoice db call".

\kreXX::timerEnd()
Ends the benchmark test and displays the result.

\kreXX::backtrace();
Outputs a backtrace with sourcecode.. When kreXX finds objects inside the
backtrace, it will analyse them.

\krexx::registerFatal();
\krexx::unregisterFatal();
Registers an unregisters the fatal error handler.

Further documentation can be found here:
href="https://sourceforge.net/p/krexx/wiki/Overview/

MAINTAINERS
-----------
Current maintainers:
* Tobias G�lzow (guelzow) - https://www.drupal.org/u/guelzow
This project has been sponsored by:
* BRAINWORXX GmbH
  We have been building internet solutions since 1999, always full of passion,
  always on the search for trends and with an eye for tiny details. We offer a
  large amount of competence, making you (more) successful in the far reaches
  of the WWW. Multimedia is much more than a business to us, it's our world.
